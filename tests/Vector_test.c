#include <criterion/criterion.h>
#include <criterion/internal/assert.h>
#include <criterion/internal/test.h>

#include "../src/libdsa.h"

TestSuite(Vector_test);

Test(Vector_test, create_vector) {
	struct Vector *test_vector = Vector_create();

	cr_expect(test_vector->length == 0,
			"test_vector->length should eqaul 0 but got %d", (int) test_vector->length);

	Vector_destroy(test_vector);
}

Test(Vector_test, Vector_is_empty) {
	struct Vector *test_vector = Vector_create();

	cr_expect(Vector_is_empty(test_vector) == 1,
			"test_vector should be empty but got %d", Vector_is_empty(test_vector));

	Vector_destroy(test_vector);
}

Test(Vector_test, Vector_append) {
	struct Vector *test_vector = Vector_create();

	int *x = malloc(sizeof(int));
	*x = 10;
	int *y = malloc(sizeof(int));
	*y = 20;
	int *z = malloc(sizeof(int));
	Vector_append(test_vector, x);
	Vector_append(test_vector, y);	
	Vector_append(test_vector, z);

	cr_expect(*(int*) test_vector->_data[0] == *x,
			"test_vector->_data[0] should equal %d but got %d", *x, *(int*) test_vector->_data[0]);
	cr_expect(*(int*) test_vector->_data[1] == *y, 
			"test_vector->_data[1] should equal %d but got %d", *y, *(int*) test_vector->_data[1]);
	cr_expect(*(int*) test_vector->_data[2] == *z,
			"test_vector->_data[2] should equal %d but got %d", *z, *(int*) test_vector->_data[2]);

	Vector_destroy(test_vector);

	test_vector = Vector_create();

	int test_vector_size = 10;

 	for (int i = 0; i < test_vector_size; i++) {
 		int *data = malloc(sizeof(int));
 		*data = i;
 		Vector_append(test_vector, data);
 	}

	for (int i = 0; i < test_vector_size; i++) {
		cr_expect(*(int*) test_vector->_data[i] == i,
				"test_vector->_data[%d] should equal %d but got %d", i, i, *(int*) test_vector->_data[i]);
	}

	Vector_destroy(test_vector);

}

Test(Vector_test, Vector_prepend) {
	struct Vector *test_vector = Vector_create();
	
	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	*x = 10, *y = 20, *z = 30;

	Vector_prepend(test_vector, &x);
	Vector_prepend(test_vector, &y);
	Vector_prepend(test_vector, &z);

	cr_expect(*(int*) test_vector->_data[0] == *z, 
			"test_vector->_data[0] should equal %d but got %d", *z, *(int*) test_vector->_data[0]);
	cr_expect(*(int*) test_vector->_data[1] == *y,
			"test_vector->_data[1] should equal %d but got %d", *y, *(int*) test_vector->_data[1]);
	cr_expect(*(int*) test_vector->_data[2] == *x,
			"test_vector->_data[2] should equal %d but got %d", *x, *(int*) test_vector->_data[2]);


	Vector_destroy(test_vector);
}

Test(Vector_test, Vector_pop_tail) {
	struct Vector *test_vector = Vector_create();
	
	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	*x = 10, *y = 20, *z = 30;
	Vector_append(test_vector, x);
	Vector_append(test_vector, y);
	Vector_append(test_vector, z);

	cr_expect(*(int*) Vector_pop_tail(test_vector) == *(int*) z,
			"Vector_pop_tail(test_vector) should equal %d but got %d", *(int*) x, *(int*) Vector_pop_tail(test_vector));
	cr_expect(*(int*) Vector_pop_tail(test_vector) == *(int*)  y,
			"Vector_pop_tail(test_vector) should equal %d but got %d", *(int*) y, *(int*) Vector_pop_tail(test_vector));
	cr_expect(Vector_pop_tail(test_vector) == x,
			"Vector_pop_tail(test_vector) should equal %d but got %d", *(int*) z, *(int*) Vector_pop_tail(test_vector));
	cr_expect_null(Vector_pop_tail(test_vector),
			"Vector_pop_tail(test_vector) should equal NULL but got %p", Vector_pop_tail(test_vector));	
	Vector_destroy(test_vector);
}

Test(Vector_test, Vector_pop_head) {
	struct Vector *test_vector = Vector_create();
	
	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	*x = 10, *y = 20, *z = 30;
	Vector_append(test_vector, x);
	Vector_append(test_vector, y);
	Vector_append(test_vector, z);

	cr_expect(*(int*) Vector_pop_head(test_vector) == *(int*) x,
			"Vector_pop_head(test_vector) should equal %d but got %d", *(int*) x, *(int*) Vector_pop_head(test_vector));
	cr_expect(*(int*) Vector_pop_head(test_vector) == *(int*) y,
			"Vector_pop_head(test_vector) should equal %d but got %d", *(int*) y, *(int*) Vector_pop_head(test_vector));
	cr_expect(*(int*) Vector_pop_head(test_vector) == *(int*) z,
			"Vector_pop_head(test_vector) should equal %d but got %d", *(int*) z, *(int*) Vector_pop_head(test_vector));
	cr_expect_null(Vector_pop_head(test_vector),
			"Vector_pop_head(test_vector) should equal NULL but got %p", Vector_pop_head(test_vector));

	Vector_destroy(test_vector);
}

Test(Vector_test, Vector_clear) {
	struct Vector *test_vector = Vector_create();
	int *x = malloc(sizeof(int));	
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	*x = 10, *y = 20, *z = 30;

	Vector_append(test_vector, x);
	Vector_append(test_vector, y);
	Vector_append(test_vector, z);

	Vector_clear(test_vector);

	cr_expect(Vector_is_empty(test_vector) == 1,
			"test_vector should be empty but got %d", Vector_is_empty(test_vector));

	Vector_destroy(test_vector);
}
Test(Vector_test, Vector_get) {
	struct Vector *test_vector = Vector_create();
	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	*x = 10, *y = 20, *z = 30;

	Vector_append(test_vector, x);
	Vector_append(test_vector, y);
	Vector_append(test_vector, z);

	cr_expect(*(int*) Vector_get(test_vector, 0) == *(int*) x,
			"Vector_get(test_vector, 0) should equal %d but got %d", *(int*) x, *(int*) Vector_get(test_vector, 0));
	cr_expect(*(int*) Vector_get(test_vector, 1) == *(int*) y,
			"Vector_get(test_vector, 1) should equal %d but got %d", *(int*) y, *(int*) Vector_get(test_vector, 1));
	cr_expect(*(int*) Vector_get(test_vector, 2) == *(int*) z,
			"Vector_get(test_vector, 2) should equal %d but got %d", *(int*) z, *(int*) Vector_get(test_vector, 2));
	cr_expect_null(Vector_get(test_vector, 3),
			"Vector_get(test_vector, 3) should equal NULL but got %p", Vector_get(test_vector, 3));
	Vector_destroy(test_vector);
}

Test(Vector_test, Vector_set) {
	struct Vector *test_vector = Vector_create();
	
	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	*x = 10, *y = 20, *z = 30;

	Vector_append(test_vector, x);
	Vector_append(test_vector, y);
	Vector_append(test_vector, z);
	
	int *a = malloc(sizeof(int));
	int *b = malloc(sizeof(int));
	int *c = malloc(sizeof(int));
	*a = 40, *b = 50, *c = 60;

	Vector_set(test_vector, 0, a);
	Vector_set(test_vector, 1, b);
	Vector_set(test_vector, 2, c);
	cr_expect(*(int*) test_vector->_data[0] == *(int*) a,
			"test_vector->_data[0] should equal %d but got %d", *(int*) a, *(int*) test_vector->_data[0]);

	Vector_destroy(test_vector);
}

Test(Vector_test, Vector_insert) {
	struct Vector *test_vector = Vector_create();
	
	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	*x = 10, *y = 20, *z = 30;

	Vector_append(test_vector, x);
	Vector_append(test_vector, y);
	Vector_append(test_vector, z);
	
	int *a = malloc(sizeof(int));
	int *b = malloc(sizeof(int));
	int *c = malloc(sizeof(int));
	*a = 40, *b = 50, *c = 60;

	Vector_insert(test_vector, 0, a);
	Vector_insert(test_vector, 1, b);
	Vector_insert(test_vector, 2, c);
	cr_expect(*(int*) test_vector->_data[0] == *(int*) a,
			"test_vector->_data[0] should equal %d but got %d", *(int*) a, *(int*) test_vector->_data[0]);
	cr_expect(*(int*) test_vector->_data[1] == *(int*) b,
			"test_vector->_data[1] should equal %d but got %d", *(int*) b, *(int*) test_vector->_data[1]);
	cr_expect(*(int*) test_vector->_data[2] == *(int*) c,
			"test_vector->_data[2] should equal %d but got %d", *(int*) c, *(int*) test_vector->_data[2]);
	cr_expect(*(int*) test_vector->_data[3] == *(int*) x,
			"test_vector->_data[3] should equal %d but got %d", *(int*) x, *(int*) test_vector->_data[3]);
	cr_expect(*(int*) test_vector->_data[4] == *(int*) y,
			"test_vector->_data[4] should equal %d but got %d", *(int*) y, *(int*) test_vector->_data[4]);

	Vector_destroy(test_vector);
}

int index_of_int(void *a) {
	return *(int*) a == 20;
}

Test(Vector_test, Vector_index_of) {
	struct Vector *test_vector = Vector_create();
	
	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	*x = 10, *y = 20, *z = 30;

	Vector_append(test_vector, x);
	Vector_append(test_vector, y);
	Vector_append(test_vector, z);
	
	cr_expect(Vector_index_of(test_vector, &index_of_int) == 1,
			"Vector_index_of(test_vector, &index_of_int) should equal 1 but got %d", Vector_index_of(test_vector, &index_of_int));
	Vector_destroy(test_vector);
}

Test(Vector_test, Vector_remove) {
	struct Vector *test_vector = Vector_create();
	
	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	*x = 10, *y = 20, *z = 30;

	Vector_append(test_vector, x);
	Vector_append(test_vector, y);
	Vector_append(test_vector, z);
	
	Vector_remove(test_vector, 1);
	cr_expect(*(int*) test_vector->_data[0] == *(int*) x,
			"test_vector->_data[0] should equal %d but got %d", *(int*) x, *(int*) test_vector->_data[0]);
	cr_expect(*(int*) test_vector->_data[1] == *(int*) z,
			"test_vector->_data[1] should equal %d but got %d", *(int*) z, *(int*) test_vector->_data[1]);

	Vector_destroy(test_vector);
}

Test(Vector_test, Vector_remove_range) {
	struct Vector *test_vector = Vector_create();

	int x = 10, y = 20, z = 30, a = 40, b = 50, c = 60;
	Vector_append(test_vector, &x);
	Vector_append(test_vector, &y);
	Vector_append(test_vector, &z);
	Vector_append(test_vector, &a);
	Vector_append(test_vector, &b);
	Vector_append(test_vector, &c);

	struct Vector *sliced_vector = Vector_remove_range(test_vector, 0, 3);

	cr_expect(sliced_vector->_data[0] == &x,
			"sliced_vector->_data[0] should equal &x but got %d", *(int*) sliced_vector->_data[0]);
	cr_expect(sliced_vector->_data[1] == &y,
			"sliced_vector->_data[1] should equal &y but got %d", *(int*) sliced_vector->_data[1]);
	cr_expect(sliced_vector->_data[2] == &z,
			"sliced_vector->_data[2] should equal &z but got %d", *(int*) sliced_vector->_data[2]);
	cr_expect(test_vector->_data[0] == &a,
			"test_vector->_data[0] should equal &a but got %d", *(int*) test_vector->_data[0]);
	cr_expect(test_vector->_data[1] == &b,
			"test_vector->_data[1] should equal &b but got %d", *(int*) test_vector->_data[1]);
	cr_expect(test_vector->_data[2] == &c,
			"test_vector->_data[2] should equal &c but got %d", *(int*) test_vector->_data[2]);

	Vector_destroy(test_vector);
	Vector_destroy(sliced_vector);
}

Test(Vector_test, Vector_reverse) {
	struct Vector *test_vector = Vector_create();

	int x = 10, y = 20, z = 30, a = 40, b = 50, c = 60;
	Vector_append(test_vector, &x);
	Vector_append(test_vector, &y);
	Vector_append(test_vector, &z);
	Vector_append(test_vector, &a);
	Vector_append(test_vector, &b);
	Vector_append(test_vector, &c);

	Vector_reverse(test_vector);

	cr_expect(test_vector->_data[0] == &c,
			"test_vector->_data[0] should equal &c but got %d", *(int*) test_vector->_data[0]);
	cr_expect(test_vector->_data[1] == &b,
			"test_vector->_data[1] should equal &b but got %d", *(int*) test_vector->_data[1]);
	cr_expect(test_vector->_data[2] == &a,
			"test_vector->_data[2] should equal &a but got %d", *(int*) test_vector->_data[2]);
	cr_expect(test_vector->_data[3] == &z,
			"test_vector->_data[3] should equal &z but got %d", *(int*) test_vector->_data[3]);
	cr_expect(test_vector->_data[4] == &y,
			"test_vector->_data[4] should equal &y but got %d", *(int*) test_vector->_data[4]);
	cr_expect(test_vector->_data[5] == &x,
			"test_vector->_data[5] should equal &x but got %d", *(int*) test_vector->_data[5]);

	Vector_destroy(test_vector);
}

int sort_int(void *a, void *b) {
	return *(int*) a - *(int*) b;
}

Test(Vector_test, Vector_sort) {
	struct Vector *test_vector = Vector_create();

	int x = 10, y = 5, z = 15, a = 8, b = 20, c = 1; 
	Vector_append(test_vector, &x);
	Vector_append(test_vector, &y);
	Vector_append(test_vector, &z);
	Vector_append(test_vector, &a);
	Vector_append(test_vector, &b);
	Vector_append(test_vector, &c);

	Vector_sort(test_vector, &sort_int);
	for (int i = 1; i < test_vector->length; i++) {
		cr_expect(test_vector->_data[i] >= test_vector->_data[i - 1],
				"test_vector->_data[%d] should be greater than or equal to test_vector->_data[%d] but got %d and %d",
				i, i - 1, *(int*) test_vector->_data[i], *(int*) test_vector->_data[i - 1]);
	}
	Vector_destroy(test_vector);
}

Test(Vector_test, Vector_copy) {
	struct Vector *test_vector = Vector_create();

	int x = 100, y = 200, z = 300, a = 400, b = 500, c = 600;
	Vector_append(test_vector, &x);
	Vector_append(test_vector, &y);
	Vector_append(test_vector, &z);
	Vector_append(test_vector, &a);
	Vector_append(test_vector, &b);
	Vector_append(test_vector, &c);

	struct Vector *copied_vector = Vector_copy(test_vector);

	cr_expect(copied_vector->_data[0] == &x,
			"copied_vector->_data[0] should equal &x but got %d", *(int*) copied_vector->_data[0]);
	cr_expect(copied_vector->_data[1] == &y,
			"copied_vector->_data[1] should equal &y but got %d", *(int*) copied_vector->_data[1]);
	cr_expect(copied_vector->_data[2] == &z,
			"copied_vector->_data[2] should equal &z but got %d", *(int*) copied_vector->_data[2]);
	cr_expect(copied_vector->_data[3] == &a,
			"copied_vector->_data[3] should equal &a but got %d", *(int*) copied_vector->_data[3]);
	cr_expect(copied_vector->_data[4] == &b,
			"copied_vector->_data[4] should equal &b but got %d", *(int*) copied_vector->_data[4]);
	cr_expect(copied_vector->_data[5] == &c,
			"copied_vector->_data[5] should equal &c but got %d", *(int*) copied_vector->_data[5]);

	for (int i = 0; i < copied_vector->length; i++) {
		copied_vector->_data[i] = (void*) &i; 
		cr_expect(test_vector->_data[i] != copied_vector->_data[i],
				"test_vector->_data[%d] should not equal copied_vector->_data[%d] but got %d and %d",
				i, i, *(int*) test_vector->_data[i], *(int*) copied_vector->_data[i]);
	}
	

	Vector_destroy(test_vector);
	Vector_destroy(copied_vector);
}

Test(Vector_test, Vector_concat) {
	struct Vector *test_vector = Vector_create();
	struct Vector *test_vector_two = Vector_create();

	int x = 100, y = 200, z = 300, a = 400, b = 500, c = 600;
	Vector_append(test_vector, &x);
	Vector_append(test_vector, &y);
	Vector_append(test_vector, &z);
	Vector_append(test_vector_two, &a);
	Vector_append(test_vector_two, &b);
	Vector_append(test_vector_two, &c);

	struct Vector *concatenated_vector = Vector_concat(test_vector, test_vector_two);

	cr_expect(concatenated_vector->_data[0] == &x,
			"concatenated_vector->_data[0] should equal &x but got %d", *(int*) concatenated_vector->_data[0]);
	cr_expect(concatenated_vector->_data[1] == &y,
			"concatenated_vector->_data[1] should equal &y but got %d", *(int*) concatenated_vector->_data[1]);
	cr_expect(concatenated_vector->_data[2] == &z,
			"concatenated_vector->_data[2] should equal &z but got %d", *(int*) concatenated_vector->_data[2]);
	cr_expect(concatenated_vector->_data[3] == &a,
			"concatenated_vector->_data[3] should equal &a but got %d", *(int*) concatenated_vector->_data[3]);
	cr_expect(concatenated_vector->_data[4] == &b,
			"concatenated_vector->_data[4] should equal &b but got %d", *(int*) concatenated_vector->_data[4]);
	cr_expect(concatenated_vector->_data[5] == &c,
			"concatenated_vector->_data[5] should equal &c but got %d", *(int*) concatenated_vector->_data[5]);

	Vector_destroy(test_vector);
	Vector_destroy(test_vector_two);
	Vector_destroy(concatenated_vector);
}

void assert_int(void *_data) {
	cr_assert(*(int*) _data == 100 || *(int*) _data == 200 || *(int*) _data == 300 || *(int*) _data == 400 || *(int*) _data == 500 || *(int*) _data == 600,
			"*(int*) _data should equal 100, 200, 300, 400, 500, or 600 but got %d", *(int*) _data);
}

Test(Vector_test, Vector_for_each) {
	struct Vector *test_vector = Vector_create();

	int x = 100, y = 200, z = 300, a = 400, b = 500, c = 600;
	Vector_append(test_vector, &x);
	Vector_append(test_vector, &y);
	Vector_append(test_vector, &z);
	Vector_append(test_vector, &a);
	Vector_append(test_vector, &b);
	Vector_append(test_vector, &c);

	Vector_for_each(test_vector, &assert_int);

	Vector_destroy(test_vector);
}

void *square_int(void *_data) {
	int *squared_int = malloc(sizeof(int));
	*squared_int = *(int*) _data * *(int*) _data;
	return squared_int;
}

Test(Vector_test, Vector_map) {
	struct Vector *test_vector = Vector_create();

	int x = 100, y = 200, z = 300, a = 400, b = 500, c = 600;
	Vector_append(test_vector, &x);
	Vector_append(test_vector, &y);
	Vector_append(test_vector, &z);
	Vector_append(test_vector, &a);
	Vector_append(test_vector, &b);
	Vector_append(test_vector, &c);

	struct Vector *mapped_vector = Vector_map(test_vector, &square_int);

	cr_expect(*(int*) mapped_vector->_data[0] == 10000,
			"*(int*) mapped_vector->_data[0] should equal 10000 but got %d", *(int*) mapped_vector->_data[0]);
	cr_expect(*(int*) mapped_vector->_data[1] == 40000,
			"*(int*) mapped_vector->_data[1] should equal 40000 but got %d", *(int*) mapped_vector->_data[1]);
	cr_expect(*(int*) mapped_vector->_data[2] == 90000,
			"*(int*) mapped_vector->_data[2] should equal 90000 but got %d", *(int*) mapped_vector->_data[2]);
	cr_expect(*(int*) mapped_vector->_data[3] == 160000,
			"*(int*) mapped_vector->_data[3] should equal 160000 but got %d", *(int*) mapped_vector->_data[3]);
	cr_expect(*(int*) mapped_vector->_data[4] == 250000,
			"*(int*) mapped_vector->_data[4] should equal 250000 but got %d", *(int*) mapped_vector->_data[4]);
	cr_expect(*(int*) mapped_vector->_data[5] == 360000,
			"*(int*) mapped_vector->_data[5] should equal 360000 but got %d", *(int*) mapped_vector->_data[5]);

	Vector_destroy(test_vector);
	Vector_destroy(mapped_vector);
}

int filter_negetive_ints(void *_data) {
	return *(int*) _data >= 0;
}

Test(Vector_test, Vector_filter) {
	struct Vector *test_vector = Vector_create();

	int x = -50, y = 100, z = -25, a = 200, b = -75, c = 300;
	Vector_append(test_vector, &x);
	Vector_append(test_vector, &y);
	Vector_append(test_vector, &z);
	Vector_append(test_vector, &a);
	Vector_append(test_vector, &b);
	Vector_append(test_vector, &c);

	struct Vector *filtered_vector = Vector_filter(test_vector, &filter_negetive_ints);

	for (int i = 0; i < filtered_vector->length; i++) {
		cr_expect(*(int*) filtered_vector->_data[i] >= 0,
				"*(int*) filtered_vector->_data[%d] should be greater than or equal to 0 but got %d", i, *(int*) filtered_vector->_data[i]);
	}

	for (int i = 0; i < filtered_vector->length; i++) {
		filtered_vector->_data[i] = (void*) &i;
		cr_expect(*(int*) filtered_vector->_data[i] != *(int*) test_vector->_data[i],
				"*(int*) filtered_vector->_data[%d] should not equal *(int*) test_vector->_data[%d] but got %d and %d",
				i, i, *(int*) filtered_vector->_data[i], *(int*) test_vector->_data[i]);
	}

	Vector_destroy(test_vector);
	Vector_destroy(filtered_vector);
}

void *sum_ints(void *_data, void *memo) {
	*(int*) memo += *(int*) _data;
	return memo;
}

Test(Vector_test, Vector_reduce) {
	struct Vector *test_vector = Vector_create();

	int x = 100, y = 200, z = 300, a = 400, b = 500, c = 600;
	Vector_append(test_vector, &x);
	Vector_append(test_vector, &y);
	Vector_append(test_vector, &z);
	Vector_append(test_vector, &a);
	Vector_append(test_vector, &b);
	Vector_append(test_vector, &c);
	
	void *initial_value = malloc(sizeof(int));
	*(int*) initial_value = 0;
	int sum = *(int*) Vector_reduce(test_vector, &sum_ints, initial_value);

	cr_expect(sum == 2100,
			"sum should equal 2100 but got %d", sum);

	Vector_destroy(test_vector);
}

Test(Vector_test, Vector_some) {
	struct Vector *test_vector = Vector_create();

	int x = -50, y = 100, z = -25, a = 200, b = -75, c = 300;
	Vector_append(test_vector, &x);
	Vector_append(test_vector, &y);
	Vector_append(test_vector, &z);
	Vector_append(test_vector, &a);
	Vector_append(test_vector, &b);
	Vector_append(test_vector, &c);

	cr_expect(Vector_some(test_vector, &filter_negetive_ints) == 1,
			"Vector_some(test_vector, &filter_negetive_ints) should equal 1 but got %d", Vector_some(test_vector, &filter_negetive_ints));

	Vector_destroy(test_vector);
}

Test(Vector_test, Vector_every) {
	struct Vector *test_vector = Vector_create();

	int x = -50, y = 100, z = -25, a = 200, b = -75, c = 300;
	Vector_append(test_vector, &x);
	Vector_append(test_vector, &y);
	Vector_append(test_vector, &z);
	Vector_append(test_vector, &a);
	Vector_append(test_vector, &b);
	Vector_append(test_vector, &c);

	cr_expect(Vector_every(test_vector, &filter_negetive_ints) == 0,
			"Vector_every(test_vector, &filter_negetive_ints) should equal 0 but got %d", Vector_every(test_vector, &filter_negetive_ints));

	Vector_destroy(test_vector);
}
