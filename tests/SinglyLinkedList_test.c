#include <criterion/criterion.h>
#include <stdlib.h>
#include "../src/libdsa.h"

TestSuite(SinglyLinkedList_test);

Test(SinglyLinkedList_test, SinglyLinkedList_is_empty) {
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();
	int result = SinglyLinkedList_is_empty(test_list);
	cr_expect(result == 1, "list should be empty, but SinglyLinkedList_is_empty returned %d", result);
	SinglyLinkedList_destroy(test_list);
}

Test(SinglyLinkedList_test, SinglyLinkedList_append) {
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();
	
	int *x = malloc(sizeof(int));
	*x = 10;
	SinglyLinkedList_append(test_list, x);
	cr_expect(*(int*) test_list->head->data == 10, 
			"list head should be 10 but is %d", *(int*) test_list->head->data);
	cr_expect(*(int*) test_list->tail->data == 10,
			"list tail should be 10 but is %d", *(int*) test_list->tail->data);
	
	int *y = malloc(sizeof(int));
	*y = 13;
	SinglyLinkedList_append(test_list, y);
	cr_expect(*(int*) test_list->head->data == 10,
			"list head should be 13 but is %d", *(int*) test_list->head->data);
	cr_expect(*(int*) test_list->tail->data == 13,
			"list tail should be 10 but is %d", *(int*) test_list->tail->data);
	
	int *z = malloc(sizeof(int));
	*z = 7;
	SinglyLinkedList_append(test_list, z);
	cr_expect(*(int*) test_list->head->data == 10, 
			"list head should be 10 but is %d", *(int*) test_list->head->data);
	cr_expect(*(int*) test_list->tail->data == 7,
			"list tail should be 7 but is %d", *(int*) test_list->tail->data);

	SinglyLinkedList_destroy(test_list);
}

Test(SinglyLinkedList_test, SinglyLinkedList_prepend) {
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();

	int *x = malloc(sizeof(int));
	*x = 10;
	SinglyLinkedList_prepend(test_list, x);
	cr_expect(*(int*) test_list->head->data == 10, 
			"list head should be 10 but is %d", *(int*) test_list->head->data);
	cr_expect(*(int*) test_list->tail->data == 10,
			"list tail should be 10 but is %d", *(int*) test_list->tail->data);

	int *y = malloc(sizeof(int));
	*y = 13;
	SinglyLinkedList_prepend(test_list, y);
	cr_expect(*(int*) test_list->head->data == 13,
			"list head should be 13 but is %d", *(int*) test_list->head->data);
	cr_expect(*(int*) test_list->tail->data == 10,
			"list tail should be 10 but is %d", *(int*) test_list->tail->data);

	int *z = malloc(sizeof(int));
	*z = 7;
	SinglyLinkedList_prepend(test_list, z);
	cr_expect(*(int*) test_list->head->data == 7, 
			"list head should be 10 but is %d", *(int*) test_list->head->data);
	cr_expect(*(int*) test_list->tail->data == 10,
			"list tail should be 7 but is %d", *(int*) test_list->tail->data);

	SinglyLinkedList_destroy(test_list);
}

Test(SinglyLinkedList_test, SinglyLinkedList_clear) {
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();
	int *a = malloc(sizeof(int));
    int *b = malloc(sizeof(int));
    int *c = malloc(sizeof(int));
    int *d = malloc(sizeof(int));
    int *e = malloc(sizeof(int));
    *a = 0; *b = 1; *c = 2; *d = 3; *e = 4;

	SinglyLinkedList_append(test_list, a);
	SinglyLinkedList_append(test_list, b);
	SinglyLinkedList_append(test_list, c);
	SinglyLinkedList_append(test_list, d);
	SinglyLinkedList_append(test_list, e);

	SinglyLinkedList_clear(test_list);

	cr_expect((void*) test_list->head == NULL,
			"test_list's head should be NULL but got %d", *(int*) test_list->head);
	cr_expect((void*)test_list->tail == NULL,
			"test_list's tail should be NULL but got %d", *(int*) test_list->tail);
	cr_expect(test_list->length == 0,
			"test_list's length should be 0 but got %d", test_list->length);

	SinglyLinkedList_destroy(test_list);
}

Test(SinglyLinkedList_test, SinglyLinkedList_pop_tail) {
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();

	void *result = SinglyLinkedList_pop_tail(test_list);
	cr_expect(result == NULL, 
			"SinglyLinkedList_pop_tail should return NULL when list is empty but is %d", *(int*) result);

	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	*x = 1; *y = 2; *z = 3;
	
	SinglyLinkedList_append(test_list, x);
	SinglyLinkedList_append(test_list, y);
	SinglyLinkedList_append(test_list, z);

	int pop_result = *(int*) SinglyLinkedList_pop_tail(test_list);
	cr_expect(pop_result == 3, 
			"SinglyLinkedList_pop_tail should return 3 but returned %d", pop_result);

	SinglyLinkedList_destroy(test_list);
}

Test(SinglyLinkedList_test, SinglyLinkedList_pop_head) {
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();

	void *result = SinglyLinkedList_pop_tail(test_list);
	cr_expect(result == NULL, 
			"SinglyLinkedList_pop_head should return NULL when list is empty but is %d", *(int*) result);

	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	*x = 1; *y = 2; *z = 3;
		
	SinglyLinkedList_append(test_list, &x);
	SinglyLinkedList_append(test_list, &y);
	SinglyLinkedList_append(test_list, &z);

	int pop_result = *(int*) SinglyLinkedList_pop_head(test_list);
	cr_expect(pop_result == 1, 
			"SinglyLinkedList_pop_tail should return 1 but returned %d", pop_result);
	pop_result = *(int*) SinglyLinkedList_pop_head(test_list);
	cr_expect(pop_result == 2, 
			"SinglyLinkedList_pop_tail should return 2 but returned %d", pop_result);
	pop_result = *(int*) SinglyLinkedList_pop_head(test_list);
	cr_expect(pop_result == 3, 
			"SinglyLinkedList_pop_tail should return 3 but returned %d", pop_result);

	SinglyLinkedList_destroy(test_list);
}

CREATE_SINGLY_LINKED_LIST_COPY_FUCNTION(int)

Test(SinglyLinkedList_test, SinglyLinkedList_copy) {
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();
	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	int *a = malloc(sizeof(int));
	int *b = malloc(sizeof(int));
	int *c = malloc(sizeof(int));
	*x = 1; *y = 2; *z = 3; *a = 4; *b = 5; *c = 6;

	SinglyLinkedList_append(test_list, x);
	SinglyLinkedList_append(test_list, y);
	SinglyLinkedList_append(test_list, z);
	SinglyLinkedList_append(test_list, a);
	SinglyLinkedList_append(test_list, b);
	SinglyLinkedList_append(test_list, c);

	struct SinglyLinkedList *test_list_SinglyLinkedList_copy = SinglyLinkedList_copy(test_list, &SinglyLinkedList_copy_int);

	cr_expect(*(int*) test_list_SinglyLinkedList_copy->head->data == 1, 
			"test_list_SinglyLinkedList_copy->head->data should be 1 but got %d", *(int*) test_list_SinglyLinkedList_copy->head->data);
	cr_expect(*(int*) test_list_SinglyLinkedList_copy->tail->data == 6, 
			"test_list_SinglyLinkedList_copy->tail->data should be 6 but got %d", *(int*) test_list_SinglyLinkedList_copy->tail->data);
	cr_expect(test_list_SinglyLinkedList_copy->length == 6, 
			"test_list_SinglyLinkedList_copy->length should be 6 but got %d", test_list_SinglyLinkedList_copy->length);
		

	SinglyLinkedList_destroy(test_list);
	SinglyLinkedList_destroy(test_list_SinglyLinkedList_copy);
}

void mult_int_node_by_ten(struct SinglyLinkedListNode *current_node) {
	*(int*) current_node->data *= 10;
}

void test_node_value(struct SinglyLinkedListNode *current_node) {
	cr_expect(*(int*) current_node->data == 100,
			"current_node's data should be 100 but got %d", *(int*) current_node->data);
}

Test(SinglyLinkedList_test, SinglyLinkedList_for_each) {
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();

	int *x = malloc(sizeof(int));	
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	*x = 10; *y = 10; *z = 10;

	SinglyLinkedList_append(test_list, x);
	SinglyLinkedList_append(test_list, y);
	SinglyLinkedList_append(test_list, z);

	SinglyLinkedList_for_each(test_list, &mult_int_node_by_ten);
	SinglyLinkedList_for_each(test_list, &test_node_value);

	SinglyLinkedList_destroy(test_list);
}

void *square_int(struct SinglyLinkedListNode *current_node) {
	int *result = malloc(sizeof(int));
	*result = *(int*) current_node->data * *(int*) current_node->data;
	return result;
}

Test(SinglyLinkedList_test, SinglyLinkedList_map) {
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();
	
	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	*x = 1; *y = 5; *z = 10;
	SinglyLinkedList_append(test_list, x);
	SinglyLinkedList_append(test_list, y);
	SinglyLinkedList_append(test_list, z);

	struct SinglyLinkedList *new_list = SinglyLinkedList_map(test_list, &square_int);

	cr_expect(*(int*) new_list->head->data == 1, 
			"list head should equal 1, but is %d", *(int*) new_list->head->data);
	cr_expect(*(int*) new_list->head->next->data == 25, 
			"list head->next should equal 25, but is %d", *(int*) new_list->head->data);
	cr_expect(*(int*) new_list->tail->data == 100, 
			"list tail should equal 100, but is %d", *(int*) new_list->tail->data);

	SinglyLinkedList_destroy(test_list);
	SinglyLinkedList_destroy(new_list);
}

int remove_zeros(struct SinglyLinkedListNode *current_node) {
	return *(int*) current_node->data != 0;
}

void test_list_for_zeros(struct SinglyLinkedListNode *current_node) {
	cr_expect(*(int*) current_node->data != 0,
			"current_node->data should not equal 0, but found a %d", 
			*(int*) current_node->data);
}

Test(SinglyLinkedList_test, SinglyLinkedList_filter) {
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();
	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	int *a = malloc(sizeof(int));
	int *b = malloc(sizeof(int));
	int *c = malloc(sizeof(int));
	*x = 1; *y = 0; *z = 3; *a = 0; *b = 5; *c = 6;

	SinglyLinkedList_append(test_list, x);
	SinglyLinkedList_append(test_list, a);
	SinglyLinkedList_append(test_list, y);
	SinglyLinkedList_append(test_list, b);
	SinglyLinkedList_append(test_list, z);
	SinglyLinkedList_append(test_list, c);
	
	struct SinglyLinkedList *new_list = SinglyLinkedList_filter(test_list, &remove_zeros, &SinglyLinkedList_copy_int);
	SinglyLinkedList_for_each(new_list, &test_list_for_zeros);

	SinglyLinkedList_destroy(test_list);
	SinglyLinkedList_destroy(new_list);
}

int int_above_five(struct SinglyLinkedListNode *current_node) {
	return *(int*) current_node->data > 5;
}

int int_above_ten(struct SinglyLinkedListNode *current_node) {
	return *(int*) current_node->data > 10;
}

int int_above_zero(struct SinglyLinkedListNode *current_node) {
	return *(int*) current_node->data > 0;
}

Test(SinglyLinkedList_test, SinglyLinkedList_some) {
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();
	
	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	int *a = malloc(sizeof(int));
	int *b = malloc(sizeof(int));
	int *c = malloc(sizeof(int));
	*x = 1; *y = 2; *z = 3; *a = 4; *b = 5; *c = 6;
	SinglyLinkedList_append(test_list, x);
	SinglyLinkedList_append(test_list, a);
	SinglyLinkedList_append(test_list, y);
	SinglyLinkedList_append(test_list, b);
	SinglyLinkedList_append(test_list, z);
	SinglyLinkedList_append(test_list, c);

	int test = SinglyLinkedList_some(test_list, &int_above_five);
	cr_expect(test == 1, "SinglyLinkedList_some should return 1 but got %d", test);
	int second_test = SinglyLinkedList_some(test_list, &int_above_ten);
	cr_expect(second_test == 0, "SinglyLinkedList_some should return 0 but got %d", second_test);

	SinglyLinkedList_destroy(test_list);
}

Test(SinglyLinkedList_test, SinglyLinkedList_every) {
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();
	
	int *x = malloc(sizeof(int));
	int *y = malloc(sizeof(int));
	int *z = malloc(sizeof(int));
	int *a = malloc(sizeof(int));
	int *b = malloc(sizeof(int));
	int *c = malloc(sizeof(int));
	*x = 1; *y = 2; *z = 3; *a = 4; *b = 5; *c = 6;

	SinglyLinkedList_append(test_list, x);
	SinglyLinkedList_append(test_list, a);
	SinglyLinkedList_append(test_list, y);
	SinglyLinkedList_append(test_list, b);
	SinglyLinkedList_append(test_list, z);
	SinglyLinkedList_append(test_list, c);

	int test = SinglyLinkedList_every(test_list, &int_above_five);
	cr_expect(test == 0, "SinglyLinkedList_some should return 0 but got %d", test);
	int second_test = SinglyLinkedList_some(test_list, &int_above_zero);
	cr_expect(second_test == 1, "SinglyLinkedList_some should return 1 but got %d", second_test);

	SinglyLinkedList_destroy(test_list);
}
