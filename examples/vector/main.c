#include <stdio.h>
#include "../../src/libdsa.h"
#include <stdlib.h>

CREATE_VECTOR_PRETTY_PRINT_FUNCTION(int, "%d")

int index_of_compare(void *current_data) {
	if (*(int *)current_data == 1000) {
		return 1;
	}
	return 0;
}

int main() {
    struct Vector *test_vector = Vector_create();
    if (!test_vector) {
        fprintf(stderr, "Failed to create vector\n");
        return 1;
    }

	printf("appending 100 to test_vector:\n");
    for (int i = 1; i <= 100; i++) {
        int *data = malloc(sizeof(int)); 
        if (!data) {
            fprintf(stderr, "Failed to allocate memory\n");
            Vector_destroy(test_vector);
            return 1;
        }

        *data = i;
        if (!Vector_append(test_vector, data)) {
            fprintf(stderr, "Failed to append data\n");
            free(data);
            Vector_destroy(test_vector);
            return 1;
        }
    }
	Vector_pretty_print_int(test_vector);

	printf("popping 50 off of the tail of the test_vector:\n");
	for (int i = 0; i < 50; i++) {
		void *data = Vector_pop_tail(test_vector);
		free(data);
	}
	Vector_pretty_print_int(test_vector);

	printf("popping 25 off of the head of the test_vector:\n");
	for (int i = 0; i < 25; i++) {
		void *data = Vector_pop_head(test_vector);
		free(data);
	}
	Vector_pretty_print_int(test_vector);

	printf("clearing test_vector:\n");
	if (!Vector_clear(test_vector)) {
		fprintf(stderr, "Failed to clear vector\n");
	}
	Vector_pretty_print_int(test_vector);

	printf("appending 0 - 10 to test_vector:\n");
	for (int i = 0; i <= 10; i++) {
		int *data = malloc(sizeof(int));
		if (!data) {
			fprintf(stderr, "Failed to allocate memory\n");
		}
		*data = i;
		if (!Vector_append(test_vector, data)) {
			fprintf(stderr, "Failed to append data\n");
			free(data);
		}
	}
	Vector_pretty_print_int(test_vector);

	printf("setting index 5 to 1000:\n");
	int *data = malloc(sizeof(int));
	if (!data) {
		fprintf(stderr, "Failed to allocate memory\n");
	}
	*data = 1000;
	if (!Vector_set(test_vector, 5, data)) {
		fprintf(stderr, "Failed to set data\n");
		free(data);
	}
	Vector_pretty_print_int(test_vector);

	printf("getting index 5:\n");
	int *get_data = Vector_get(test_vector, 5);
	if (!get_data) {
		fprintf(stderr, "Failed to get data\n");
	}
	printf("data at index 5: %d\n", *get_data);
	
	printf("inserting -39 at the 5th index:\n");
	int *insert_data = malloc(sizeof(int));
	if (!insert_data) {
		fprintf(stderr, "Failed to allocate memory\n");
	}
	*insert_data = -39;
	if (!Vector_insert(test_vector, 5, insert_data)) {
		fprintf(stderr, "Failed to insert data\n");
		free(insert_data);
	}
	Vector_pretty_print_int(test_vector);

	printf("inseting 100 - 110 at the 5th index:\n");
	for (int i = 100; i <= 110; i++) {
		int *data = malloc(sizeof(int));
		if (!data) {
			fprintf(stderr, "Failed to allocate memory\n");
		}
		*data = i;
		if (!Vector_insert(test_vector, 5, data)) {
			fprintf(stderr, "Failed to insert data\n");
			free(data);
		}
	}
	Vector_pretty_print_int(test_vector);

	printf("finding index of 1000:\n");
	int index = Vector_index_of(test_vector, &index_of_compare);
	printf("index of 1000: %d\n", index);

	printf("removing index 5:\n");
	void *remove_data = Vector_remove(test_vector, 5);
	printf("removed data: %d\n", *(int *)remove_data);
	// free(remove_data);
	Vector_pretty_print_int(test_vector);

    Vector_destroy(test_vector);

    return 0;
}

