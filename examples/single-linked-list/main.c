#include "../../src/libdsa.h"

#include <stdio.h>
#include <stdlib.h>


CREATE_SINGLY_LINKED_LIST_PRETTY_PRINT_FUNCTION(int, "%d")
CREATE_SINGLY_LINKED_LIST_PRETTY_PRINT_FUNCTION(char, "%c")
CREATE_SINGLY_LINKED_LIST_COPY_FUCNTION(int)

int main(int argc, char *argv[]) {
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();

	size_t size = 10;
	printf("apppeding 0 - %d to test_list\n", (int) size);
	for (int i = 0; i < size; i++) {
		int *data = malloc(sizeof(int));
		*(int *) data = i;
		SinglyLinkedList_append(test_list, data);
	}	
	SinglyLinkedList_pretty_print_int(test_list);

	printf("prepending 0 to -%d to test_list\n", (int) size);
	for (int i = -size; i < 0; i++) {
		int *data = malloc(sizeof(int));
		*(int *) data = i;
		SinglyLinkedList_prepend(test_list, data);
	}
	SinglyLinkedList_pretty_print_int(test_list);

	printf("clearing list.\n");
	SinglyLinkedList_clear(test_list);
	SinglyLinkedList_pretty_print_int(test_list);

	printf("Appending 'Hello, World.' as individuals chars to test_list\n");
	char *greeting = "Hello, World.";
	for (int i = 0; greeting[i] != '\0'; ++i) {
		char *c = malloc(sizeof(char));
		*(char *) c = greeting[i];
		SinglyLinkedList_append(test_list, c);
	}
	SinglyLinkedList_pretty_print_char(test_list);
	printf("clearing list.\n");
	SinglyLinkedList_clear(test_list);

	printf("adding 100 - 1000 to list\n");
	for (int i = 1; i < 11; ++i) {
		int *data = malloc(sizeof(int));
		*(int*) data = i * 100;
		SinglyLinkedList_append(test_list, data);
	}
	SinglyLinkedList_pretty_print_int(test_list);

	int *popped_tail = (int*) SinglyLinkedList_pop_tail(test_list);
	printf("popped the tail of %d from test_list\n", *popped_tail);
	SinglyLinkedList_pretty_print_int(test_list);

	int *popped_head = SinglyLinkedList_pop_head(test_list);
	printf("popped the head of %d from test_list\n", *popped_head);
	SinglyLinkedList_pretty_print_int(test_list);

	struct SinglyLinkedList *copied_list = SinglyLinkedList_copy(test_list, &SinglyLinkedList_copy_int);
	for (struct SinglyLinkedListNode *current = copied_list->head;
			current != NULL; current = current->next) {
			*(int*) current->data = *(int*) current->data * 100;
	}
	printf("copied test_list and multiplied the values of the copy by 100\n");
	printf("test_list:\n");
	SinglyLinkedList_pretty_print_int(test_list);
	printf("copied_list:\n");
	SinglyLinkedList_pretty_print_int(copied_list);
	SinglyLinkedList_destroy(copied_list);

	SinglyLinkedList_destroy(test_list);
	return 0;
}

