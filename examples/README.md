# Examples

Each datastructure has an example program. To compile and run cd to the directory and run:

```sh
gcc main.c -g -L ../../lib -I ../../lib -l DSA && ./a.out
```
