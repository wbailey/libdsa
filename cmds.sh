# make and run exmaple file
nodemon --exec "sleep 3 && gcc main.c -g -L ../../lib -I ../../lib -l DSA && ./a.out || exit 1" -e .h,.c
# auto rebuild lib
nodemon --exec "make clean && make || exit 1" -e .h,.c
# run a specific suite of unit tests
./tests/bin/Vector_test --filter="*"
