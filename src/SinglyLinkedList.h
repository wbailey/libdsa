#ifndef _SINGLY_LINKED_LIST_H_
#define _SINGLY_LINKED_LIST_H_

#include <stdio.h>

/** @defgroup SinglyLinkedList 
 * A linked list, a node used for data values and instance methods of the list
 *
 * Each node of the list has a 'data' value that is a void *pointer. 
 * 
 * ## Usage
 * 
 * Lists can be created and freed using the `SinglyLinkedList_create()` constructor and `SinglyLinkedList_destroy()` destructor:
 *
 * ```c
 * // create a new list
 * struct SinglyLinkedList *my_list = SinglyLinkedList_create();
 * // free a list 
 * SinglyLinkedList_destroy(my_list);
 * ```
 *
 * NOTE: `SinglyLinkedList_destroy();` will free the list as well as any nodes in the list.
 *
 * All instance methods of the list take the list instance as the first argument when called:
 *
 * ```c
 * struct SinglyLinkedList *my_list = SinglyLinkedList_create();
 * // passing the 'my_list' instance to the append method along with a data value to append
 * int num = 10;
 * my_list->append(my_list, &num);
 * printf("%d\n", *(int*) my_list->tail->data); // 10
 * SinglyLinkedList_destroy(my_list);
 * ```
 *
 * The primary use case of `struct SinglyLinkedListNode` outside of `list.c` is for defining callback function interfaces to work with list methods.
 * 
 * Example of defining a callback function with a list iterator:
 *
 * ```c
 * // Define a callback function to print each element in the list.
 * void print_list_element(struct SinglyLinkedListNode *current_node) {
 *     printf("%d\n", *(int*)current_node->data);
 * }
 * // Create a list and add elements to it...
 * struct SinglyLinkedList *my_list = SinglyLinkedList_create();
 * // Add elements to my_list...
 * my_list->for_each(my_list, &print_list_element);
 * SinglyLinkedList_destroy(my_list);
 * ```
 * 
 * Example of defining a comparison function for nodes of a specific type:
 *
 * ```c
 * // Define a comparison function to compare two nodes based on their data.
 * int compare_nodes(struct SinglyLinkedListNode *node1, struct SinglyLinkedListNode *node2) {
 *     // Assuming nodes contain integers
 *     int *value1 = (int *)node1->data;
 *     int *value2 = (int *)node2->data;
 *     if (value1 != NULL && value2 != NULL) {
 *         return (*value1 > *value2) ? 1 : ((*value1 < *value2) ? -1 : 0);
 *     }
 *     return 0; // Handle errors or non-integer data gracefully.
 * }
 * // Create a list and add elements to it...
 * struct SinglyLinkedList *my_list = SinglyLinkedList_create();
 * // Sort the list using the compare_nodes function...
 * // (Assuming you have a sorting algorithm implemented)
 * // my_list->sort(my_list, &compare_nodes);
 * // Handle sorted list...
 * SinglyLinkedList_destroy(my_list);
 * ```
 *
 * @todo Methods to implement in the future for the SinglyLinkedList:
 *		- count
 *		- reverse
 *		- concat
 *		- sort
 *		- reduce
 *		- index_of
 *		- last_index_of
 *		- to_vector
 * 
 * @todo Check the tests for map and filter. Tests need to be written to ensure deep copies are made. The current tests might not be working as expected.
 * @{
 */

/**
 * @brief A singly linked list implementation.
 */

struct SinglyLinkedList {
	struct SinglyLinkedListNode *head;
	struct SinglyLinkedListNode *tail;
	int length;
};

/* constructor and destructor */
struct SinglyLinkedList *SinglyLinkedList_create(void);
void SinglyLinkedList_destroy(struct SinglyLinkedList *self);

/* instance methods */
int SinglyLinkedList_is_empty(struct SinglyLinkedList *self);
void SinglyLinkedList_append(struct SinglyLinkedList *self, void *data);
void SinglyLinkedList_prepend(struct SinglyLinkedList *self, void *data);
void SinglyLinkedList_clear(struct SinglyLinkedList *self);
void *SinglyLinkedList_pop_tail(struct SinglyLinkedList *self);
void *SinglyLinkedList_pop_head(struct SinglyLinkedList *self);
struct SinglyLinkedList *SinglyLinkedList_copy(struct SinglyLinkedList *self, 
		void* (*copy_callback)(struct SinglyLinkedListNode *));
void SinglyLinkedList_for_each(struct SinglyLinkedList *self, 
		void (*callback)(struct SinglyLinkedListNode *));
struct SinglyLinkedList *SinglyLinkedList_map(struct SinglyLinkedList *self, 
		void *(*callback)(struct SinglyLinkedListNode *)); 
struct SinglyLinkedList *SinglyLinkedList_filter(struct SinglyLinkedList *self, 
		int (*callback)(struct SinglyLinkedListNode *), 
		void *(*copy_callback)(struct SinglyLinkedListNode *));
int SinglyLinkedList_some(struct SinglyLinkedList *self, int (*callback)(struct SinglyLinkedListNode *));
int SinglyLinkedList_every(struct SinglyLinkedList *self, int (*callback)(struct SinglyLinkedListNode *));

/**
 * @brief Macro to define a copy callback function for a singly linked list.
 *
 * This macro defines a function that creates a deep copy of data for a 
 * singly linked list. The function allocates memory for a new data element, 
 * copies the contents of the input data into it, and returns a pointer to 
 * the new data. If memory allocation fails, it returns NULL.
 *
 * @param type The data type of the elements in the singly linked list. This
 *             type is used to determine the size of memory allocation and 
 *             the type casting for data copying.
 *
 * @return void * A pointer to the copied data.
 *
 * ## Example Usage
 *
 * ```c
 * // Define a copy callback function for a singly linked list of integers.
 * CREATE_SINGLY_LINKED_LIST_COPY_FUCNTION(int)
 * // Create a list and add elements to it...
 * struct SinglyLinkedList *my_list = SinglyLinkedList_create();
 * int a = 1, b = 2, c = 3;
 * SinglyLinkedList_append(my_list, &a);
 * SinglyLinkedList_append(my_list, &b);
 * SinglyLinkedList_append(my_list, &c);
 * // Create a copy of the list.
 * struct SinglyLinkedList *my_list_copy = SinglyLinkedList_copy(my_list, &SinglyLinkedList_copy_int);
 * // Handle copied list...
 * SinglyLinkedList_destroy(my_list);
 * SinglyLinkedList_destroy(my_list_copy);
 * ``` 
 */

#define CREATE_SINGLY_LINKED_LIST_COPY_FUCNTION(type) \
void *SinglyLinkedList_copy_##type(struct SinglyLinkedListNode *node) { \
	type *copied_data = malloc(sizeof(type)); \
	if (!copied_data) { \
		return NULL; \
	} \
	*copied_data = *(type*) node->data; \
	return copied_data; \
} \

/**
  * @brief Macro to define a pretty print function for a singly linked list.
  *
  * This macro defines a function that prints the contents of a singly linked
  * list to stdout. The function prints the length of the list and the data
  * values of each node in the list. The format of the data values is defined
  * by the input format string.
  *
  * @param type The data type of the elements in the singly linked list. This
  *             type is used to determine the type casting for data printing.
  * @param format The format string for printing the data values of the list.
  *
  * @return void
  *
  * ## Example Usage
  *
  * ```c
  * // Define a pretty print function for a singly linked list of integers.
  * CREATE_SINGLY_LINKED_LIST_PRETTY_PRINT_FUNCTION(int, "%d")
  * struct SinglyLinkedList *my_list = SinglyLinkedList_create();
  * int a = 1, b = 2, c = 3;
  * SinglyLinkedList_append(my_list, &a);
  * SinglyLinkedList_append(my_list, &b);
  * SinglyLinkedList_append(my_list, &c);
  * // Print the list.
  * SinglyLinkedList_pretty_print_int(my_list);
  * SinglyLinkedList_destroy(my_list);
  * ```
  */




#define CREATE_SINGLY_LINKED_LIST_PRETTY_PRINT_FUNCTION(type, format) \
void SinglyLinkedList_pretty_print_##type(struct SinglyLinkedList *list) { \
    printf("(%p) list->length: %d [ ", list, list->length); \
    for (struct SinglyLinkedListNode *current = list->head; \
			current != NULL; current = current->next) { \
        printf(format " -> ", *(type *)current->data); \
    } \
    printf("NULL ]\n"); \
}

/**
 * @brief A single node of a singly linked list for. 
 */

struct SinglyLinkedListNode {
	void *data;
	struct SinglyLinkedListNode *next;
};

/* constructor and destructor */
struct SinglyLinkedListNode *SinglyLinkedListNode_create(void *data);
void SinglyLinkedListNode_destroy(struct SinglyLinkedListNode *self);

/** @} */



#endif /* _SINGLY_LINKED_LIST_H_*/
