#ifndef _VECTOR_H_
#define _VECTOR_H_ 

#include <stdlib.h>
#include <stdio.h>

/**@defgroup Vector
  * A self-mananging resizable array.
  *
  * @{
  */

/** 
  * @brief A vector implementation.
  */

struct Vector {
	size_t length;
	size_t _max_allocated;
	void **_data;
};

struct Vector *Vector_create(void);
void Vector_destroy(struct Vector *self);

int Vector_is_empty(struct Vector *self);
int Vector_append(struct Vector *self, void *data);
int Vector_prepend(struct Vector *self, void *data);
void *Vector_pop_tail(struct Vector *self);
void *Vector_pop_head(struct Vector *self);
int Vector_clear(struct Vector *self);
void *Vector_get(struct Vector *self, int index);
int Vector_set(struct Vector *self, int index, void *data);
int Vector_insert(struct Vector *self, int index, void *data);
int Vector_index_of(struct Vector *self, int (compare)(void * current_data));
void *Vector_remove(struct Vector *self, int index);
struct Vector *Vector_remove_range(struct Vector *self, int start, int end);
void *Vector_reverse(struct Vector *self);
void *Vector_sort(struct Vector *self, int (*compare)(void * a, void * b)); 
struct Vector *Vector_copy(struct Vector *self);
struct Vector *Vector_concat(struct Vector *self, struct Vector *other);
void Vector_for_each(struct Vector *self, void (*callback)(void *));
struct Vector *Vector_map(struct Vector *self, void *(*callback)(void *));
struct Vector *Vector_filter(struct Vector *self, int (*callback)(void *));
void *Vector_reduce(struct Vector *self, void *(*callback)(void *, void *), void *initial_value);
int Vector_some(struct Vector *self, int (*callback)(void *));
int Vector_every(struct Vector *self, int (*callback)(void *));

/**
  * @brief A macro to create a vector of a specific type.
  *
  * @param type The type of the vector.
  * @param format The format to print the type.
  */

#define CREATE_VECTOR_PRETTY_PRINT_FUNCTION(type, format) \
	void Vector_pretty_print_##type(struct Vector *self) { \
		printf("(%p) vector->length: %d, vector->_max_allocated: %d, [ ", self, (int) self->length, (int) self->_max_allocated); \
		for (int i = 0; i < self->length; i++) { \
			printf(format, *(type*) Vector_get(self, i)); \
			if (i < self->length - 1) { \
				printf(", "); \
			} \
		} \
		printf(" ]\n"); \
	}
/** @} */

#endif // _VECTOR_H_
