#include "SinglyLinkedList.h"

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>


/**
 * @ingroup SinglyLinkedList
 * @brief List constructor. Create a new linked list.
 *
 * `SinglyLinkedList_create(void)` allocates memory for a new linked list and initializes
 * the members. The new linked list is empty with a length of 0.
 *
 * @return A pointer to the newly created linked list.
 *
 * ## Example Usage
 *
 * ```c
 * // create a new linked list
 * struct SinglyLinkedList *new_list = SinglyLinkedList_create();
 * new_list->length // 0
 * new_list->head // NULL
 * new_list->tail // NULL
 * ```
 */

struct SinglyLinkedList *SinglyLinkedList_create(void) {
	struct SinglyLinkedList *new_list = malloc(sizeof(struct SinglyLinkedList));
	if (!new_list) {
		return NULL;
	}
	new_list->length = 0;
	new_list->head = NULL;
	new_list->tail = NULL;
	return new_list;
}

/**
 * @ingroup SinglyLinkedList 
 * @brief List Destructor. Free a linked list.
 *
 * `SinglyLinkedList_destroy(struct SinglyLinkedList  *self)` takes a pointer to a list struct and frees
 *  it along with all nodes in the list.
 *
 * @param self A pointer to a list that will be freed.
 *
 * ## Example Usage
 *
 * ```c
 * struct SinglyLinkedList *new_list = SinglyLinkedList_create();
 * int x = 1, y = 2, z = 3;
 * SinglyLinkedList_append(new_list, &x);
 * SinglyLinkedList_append(new_list, &y);
 * SinglyLinkedList_append(new_list, &z);
 * // free list along will all data nodes in the list
 * SinglyLinkedList_destroy(new_list);
 * ```
 */

void SinglyLinkedList_destroy(struct SinglyLinkedList *self) {
	struct SinglyLinkedListNode *current_node = self->head;
	while (current_node) {
		struct SinglyLinkedListNode *next_node = current_node->next;
		SinglyLinkedListNode_destroy(current_node);
		current_node = next_node;
	}
	free(self);
}

struct SinglyLinkedListNode *SinglyLinkedListNode_create(void *data) {
	struct SinglyLinkedListNode *new_node = malloc(sizeof(struct SinglyLinkedListNode));
	if (!new_node) {
		return NULL;
	}
	new_node->data = data;
	new_node->next = NULL;
	return new_node;
}

void SinglyLinkedListNode_destroy(struct SinglyLinkedListNode *self) { 
	free(self->data);
	free(self); 
}

/**
 * @ingroup SinglyLinkedList 
 * @brief checks if the given list is empty.
 *
 * `SinglyLinkedList_is_empty(struct SinglyLinkedList *self)` is used to determine if the given list is
 * empty.
 *
 * @param self A pointer to the list which will be checked.
 *
 * @return An integer `1` if the list is empty or `0` if the list is not.
 *
 * ## Example Usage
 *
 * ```c
 * struct SinglyLinkedList *new_list = SinglyLinkedList_create();
 * SinglyLinkedList_is_empty(new_list); // 1
 * int num = 1;
 * SinglyLinkedList_append(new_list, &num);
 * SinglyLinkedList_is_empty(new_list); // 0
 * ```
 */

int SinglyLinkedList_is_empty(struct SinglyLinkedList *self) { 
	return self->length == 0; 
}

/**
 * @ingroup SinglyLinkedList 
 * @brief adds a new data node to the tail of the a list.
 *
 * `SinglyLinkedList_append(struct SinglyLinkedList *self, void *data)` Creates a new node with the
 * given data and SinglyLinkedList_appends it to the tail of the given list. Increments the length
 * of the list.
 *
 * @param self A pointer to the list which will be SinglyLinkedList_appended to.
 *
 * @param data A pointer to the data values that will be stored in the new node.
 *
 * ## Example Usage
 *
 * ```c
 * struct SinglyLinkedList *new_list = SinglyLinkedList_create();
 * int x = 1, y = 2, z = 3;
 * SinglyLinkedList_append(new_list, &x);
 * SinglyLinkedList_append(new_list, &y);
 * SinglyLinkedList_append(new_list, &z);
 * *(int*) new_list->head->data; // 1
 * *(int*) new_list->tail->data; // 3
 * ```
 */

 void SinglyLinkedList_append(struct SinglyLinkedList *self, void *data) {
	struct SinglyLinkedListNode *new_node = SinglyLinkedListNode_create(data);
	/* if this is the first value in the list */
	if (SinglyLinkedList_is_empty(self)) {
		self->head = new_node;
		self->tail = new_node;
	} else {
		self->tail->next = new_node;
		self->tail = new_node;
	}
		self->length++;
	return;
}

/**
 * @ingroup SinglyLinkedList 
 * @brief adds a new data node to the head of the a list.
 *
 * `SinglyLinkedList_prepend(struct SinglyLinkedList *self, void *data)` Creates a new node with the
 * given data and SinglyLinkedList_prepends it to the head of the given list. Increments the
 * length of the list.
 *
 * @param self A pointer to the list with will be SinglyLinkedList_prepended to.
 *
 * @param data A pointer to the data values that will be stored in the new node.
 *
 * ## Example Usage
 *
 * ```c
 * struct SinglyLinkedList *new_list = SinglyLinkedList_create();
 * int x = 1, y = 2, z = 3;
 * SinglyLinkedList_prepend(new_list, &x);
 * SinglyLinkedList_prepend(new_list, &y);
 * SinglyLinkedList_prepend(new_list, &z);
 * *(int*) new_list->head->data; // 3
 * *(int*) new_list->tail->data; // 1
 * ```
 */

 void SinglyLinkedList_prepend(struct SinglyLinkedList *self, void *data) {
	struct SinglyLinkedListNode *new_node = SinglyLinkedListNode_create(data);
	if (SinglyLinkedList_is_empty(self)) {
		self->head = new_node;
		self->tail = new_node;
	} else {
		new_node->next = self->head;
		self->head = new_node;
	}
	self->length++;
	return;
}

/**
 * @ingroup SinglyLinkedList
 * @brief clears all nodes in the list and resets its length, head, and tail.
 *
 * `SinglyLinkedList_clear(struct SinglyLinkedList *self)` SinglyLinkedList_clears all nodes in the given list and resets its length,
 * head, and tail to NULL.
 *
 * @param self A pointer to the list which will be SinglyLinkedList_cleared.
 *
 * ## Example Usage
 *
 * ```c
 * struct SinglyLinkedList *my_list = SinglyLinkedList_create();
 * // Add elements to the list...
 * SinglyLinkedList_clear(my_list);
 * // Now the list is empty with length 0.
 * ```
 */

 void SinglyLinkedList_clear(struct SinglyLinkedList *self) {
	struct SinglyLinkedListNode *current_node = self->head;
	while (current_node) {
		struct SinglyLinkedListNode *temp_node = current_node->next;
		SinglyLinkedListNode_destroy(current_node);
		current_node = temp_node;
	}
	self->length = 0;
	self->head = NULL;
	self->tail = NULL;
}


/**
 * @ingroup SinglyLinkedList 
 * @brief removes the tail node from a given list.
 *
 * `SinglyLinkedList_pop_tail(struct SinglyLinkedList *self)` - Removes the tail of the given list and
 * returns it. Decrements the length of the list.
 *
 * @param self A pointer to the list which will be SinglyLinkedList_prepended to.
 *
 * @return A void (void *) pointer of the data that was stored in the tail node.
 * Will be `NULL` if the list that is popped from is empty.
 *
 * ## Example Usage
 *
 * ```c
 * struct SinglyLinkedList *new_list = SinglyLinkedList_create();
 * int x = 1, y = 2, z = 3;
 * SinglyLinkedList_append(new_list, &x);
 * SinglyLinkedList_append(new_list, &y);
 * SinglyLinkedList_append(new_list, &z);
 * *(int*) new_list->SinglyLinkedList_pop_tail(new_list); // 3
 * *(int*) new_list->tail->data; // 2
 * ```
 */

 void *SinglyLinkedList_pop_tail(struct SinglyLinkedList *self) {
	if (SinglyLinkedList_is_empty(self)) {
		return NULL;
	} else if (self->length == 1) {
		void *temp = self->tail->data;
		SinglyLinkedListNode_destroy(self->tail);
		self->tail = NULL;
		self->head = NULL;
		self->length--;
		return temp;
	} else {
		struct SinglyLinkedListNode *current_node = self->head;
		while (current_node->next->next) {
			current_node = current_node->next;
		}
		void *temp = current_node->next->data;
		SinglyLinkedListNode_destroy(current_node->next);
		current_node->next = NULL;
		self->tail = current_node;
		self->length--;
		return temp;
	}
}

/**
 * @ingroup SinglyLinkedList 
 * @brief Instance method that removes the head node from a given list.
 *
 * `SinglyLinkedList_pop_head(struct SinglyLinkedList *self)` - Removes the head of the given list and
 * returns it. Decrements the length of the list.
 *
 * @param self A pointer to the list which will be SinglyLinkedList_prepended to.
 *
 * @return A void (void *) pointer of the data that was removed from the head
 * node. Will be `NULL` if the list that is popped from is empty.
 *
 * ## Example Usage
 *
 * ```c
 * struct SinglyLinkedList *new_list = SinglyLinkedList_create();
 * int x = 1, y = 2, z = 3;
 * SinglyLinkedList_append(new_list, &x);
 * SinglyLinkedList_append(new_list, &y);
 * SinglyLinkedList_append(new_list, &z);
 * *(int*) SinglyLinkedList_pop_head(new_list); // 1
 * *(int*) new_list->head->data; // 2
 * ```
 */

 void *SinglyLinkedList_pop_head(struct SinglyLinkedList *self) {
	if (SinglyLinkedList_is_empty(self)) {
		return NULL;
	} else if (self->length == 1) {
		void *temp = self->head->data;
		SinglyLinkedListNode_destroy(self->head);
		self->head = NULL;
		self->tail = NULL;
		self->length--;
		return temp;
	} else {
		void *temp_data = self->head->data;
		struct SinglyLinkedListNode *temp_node = self->head;
		self->head = self->head->next;
		SinglyLinkedListNode_destroy(temp_node);
		self->length--;
		return temp_data;
	}
}

/** 
  * @ingroup SinglyLinkedList
  * @brief Copies a given list.
  *
  * `struct SinglyLinkedList SinglyLinkedList_copy(struct SinglyLinkedList *self, void *(callback)(void *))` creates a new list and copies the data from the given list into the new list. The callback function is invoked on each node of the given list, and the return value of the callback function is stored in the new list. The new list is returned from `SinglyLinkedList_copy()`.
  *
  * @param self A pointer to the list that will be copied.
  *
  * @param callback A pointer to a function with an interface of `void *(void *)` that will be called on every node to copy the data.
  *
  * @return A pointer to a new list containing the copied data or NULL if an error occurs.
  *
  * ## Example Usage
  *
  * ```c
  * // Define a callback function to copy the list (example: copy an integer).
  * CREATE_SINGLY_LINKED_LIST_COPY_FUCNTION(int)
  * struct SinglyLinkedList *original_list = SinglyLinkedList_create();
  * int x = 1, y = 2, z = 3;
  * SinglyLinkedList_append(original_list, &x);
  * SinglyLinkedList_append(original_list, &y);
  * SinglyLinkedList_append(original_list, &z);
  * struct SinglyLinkedList *copied_list = SinglyLinkedList_copy(original_list, &SinglyLinkedList_copy_int);
  * // Now copied_list contains the same values as original_list.
  * ```
  */

 struct SinglyLinkedList *SinglyLinkedList_copy(struct SinglyLinkedList *self, void *(copy_callback)(struct SinglyLinkedListNode *node)) {
	struct SinglyLinkedList *new_list = SinglyLinkedList_create();
	for (struct SinglyLinkedListNode *current_node = self->head; current_node != NULL;
			current_node = current_node->next) {
		void *copied_data = copy_callback(current_node);
		if (!copied_data) {
			return NULL;
		}
		SinglyLinkedList_append(new_list, copied_data);
	}
	return new_list;
}

/**
 * @ingroup SinglyLinkedList 
 * @brief invokes a callback function on every node of the
 * given list.
 *
 * `SinglyLinkedList_for_each(struct SinglyLinkedList *self, void (*callback)(struct (node*))` Iterates
 * the given list invoking the given callback function on every node of the list.
 * The current node is passed into the callback function each iteration.
 *
 * @param self A pointer to the list that will have the callback function invoked 
 * on each of it's nodes.
 *
 * @param callback A pointer to a function with an interface of `void (struct
 * *node)` that will be invoked on every node.
 *
 * ## Example Usage
 *
 * ```c
 * struct SinglyLinkedList *new_list = SinglyLinkedList_create();
 * // define a callback function using the `struct SinglyLinkedListNode`
 * void print_list(struct SinglyLinkedListNode *current_node) {
 *		printf("%d", *(int*) current_node->data);
 *     if (current_node->data != NULL) {
 *			printf(" -> ");
 *		} else {
 *			printf("\n");
 *		}
 * }
 * int x = 1, y = 2, z = 3;
 * SinglyLinkedList_append(new_list, &x);
 * SinglyLinkedList_append(new_list, &y);
 * SinglyLinkedList_append(new_list, &z);
 * SinglyLinkedList_for_each(&print_list); // 1 -> 2 -> 3
 * ```
 */

 void SinglyLinkedList_for_each(struct SinglyLinkedList *self, void (*callback)(struct SinglyLinkedListNode *)) {
  for (struct SinglyLinkedListNode *current_node = self->head; current_node != NULL;
       current_node = current_node->next) {
    callback(current_node);
  }
  return;
}

/**
 * @ingroup SinglyLinkedList 
 * @brief maps a given list to a new list. 
 * 
 * `struct SinglyLinkedList SinglyLinkedList_map(struct SinglyLinkedList *self, void *(*callback)(struct SinglyLinkedListNode *))` iterates the given list invoking the given callback function on each node of the list. The current node is passed into the callback function each iteration, and the return value of the callback function is added to a new list. The new list is returned from `SinglyLinkedList_map()`.
 * 
 * @param self A pointer to the list that will be mapped to the new list. 
 * 
 * @param callback A pointer to a function with an interface of `void *(struct SinglyLinkedListNode *)` that will be called on every node.
 * 
 * @return A pointer to a new list containing the mapped data or NULL if an error occurs.
 *
 * ## Example Usage
 *
 * ```c
 * struct SinglyLinkedList *original_list = SinglyLinkedList_create();
 * // Add elements to the original_list...
 * 
 * // Define a callback function to SinglyLinkedList_map the list (example: square the integers).
 * void *square_data(struct SinglyLinkedListNode *current_node) {
 *     int *value = (int *)current_node->data;
 *     if (value != NULL) {
 *         int *result = malloc(sizeof(int));
 *         if (result != NULL) {
 *             *result = (*value) * (*value);
 *             return result;
 *         }
 *     }
 *     return NULL; // SinglyLinkedList_map will handle errors if NULL is returned.
 * }
 * 
 * struct SinglyLinkedList mapped_list = SinglyLinkedList_map(original_list, &square_data);
 * // Now SinglyLinkedList_mapped_list contains squared values of the original list's elements.
 * ```
 */

 struct SinglyLinkedList *SinglyLinkedList_map(struct SinglyLinkedList *self, void *(*callback)(struct SinglyLinkedListNode *)) {
	struct SinglyLinkedList *new_list = SinglyLinkedList_create();
	for (struct SinglyLinkedListNode *current_node = self->head; current_node != NULL;
			current_node = current_node->next) {
		void *SinglyLinkedList_mapped_data = callback(current_node);
		if (!SinglyLinkedList_mapped_data) {
			SinglyLinkedList_destroy(new_list);
			return NULL;
		}
		SinglyLinkedList_append(new_list, SinglyLinkedList_mapped_data);
	}
	return new_list;
}

/**
 * @ingroup SinglyLinkedList
 * @brief filter values out of a given list into a new list.
 *
 * `struct SinglyLinkedList SinglyLinkedList_filter(struct SinglyLinkedList *self, int (*callback)(struct SinglyLinkedListNode *))` iterates a given list invoking the callback function on each node of the list, passing the current node in as it iterates. If the callback function returns `0`, the current node is not added to the new list. If the callback returns a non-zero number, the current node is added to the new list. The new list is returned once iteration is complete.
 *
 * @param self A pointer to the list that will be SinglyLinkedList_filtered.
 *
 * @param callback A pointer to a function with an interface `int (struct SinglyLinkedListNode *)` that will be called on every node.
 *
 * @return A pointer to a new list containing the filtered data.
 *
 * ## Example Usage
 *
 * ```c
 * struct SinglyLinkedList *original_list = SinglyLinkedList_create();
 * // Add elements to the original_list...
 *
 * // Define a callback function to SinglyLinkedList_filter the list (example: SinglyLinkedList_filter even numbers).
 * int is_even(struct SinglyLinkedListNode *current_node) {
 *     int *value = (int *)current_node->data;
 *     if (value != NULL) {
 *         return (*value % 2) == 0;
 *     }
 *     return 0; 
 * }
 *
 * struct SinglyLinkedList *SinglyLinkedList_filtered_list = original_list->SinglyLinkedList_filter(original_list, &is_even);
 * // Now SinglyLinkedList_filtered_list contains only the even numbers from the original list.
 * ```
 */

 struct SinglyLinkedList *SinglyLinkedList_filter(struct SinglyLinkedList *self, 
		 int (*callback)(struct SinglyLinkedListNode *),
		 void *(*copy_callback)(struct SinglyLinkedListNode *)) {
	struct SinglyLinkedList *new_list = SinglyLinkedList_create();
	for (struct SinglyLinkedListNode *current_node = self->head; current_node != NULL;
			current_node = current_node->next) {
		if (callback(current_node)) {
			void *copied_data = copy_callback(current_node);
			if (!copied_data) {
				SinglyLinkedList_destroy(new_list);
				return NULL;
			}
			SinglyLinkedList_append(new_list, copied_data);
		}
	}
	return new_list;	
}

/**
 * @ingroup SinglyLinkedList
 * @brief Instance method that tests whether some nodes of the list pass the test provided by the given function.
 *
 * `int SinglyLinkedList_some(struct SinglyLinkedList *self, int (*callback)(struct SinglyLinkedListNode *))` tests whether any nodes in the given list pass the test provided by the callback function. It iterates through the list, invoking the callback function on each node. If the callback function returns a non-zero value for any node, this function returns `1`, indicating that SinglyLinkedList_some nodes pass the test. Otherwise, it returns `0`, indicating that none of the nodes pass the test.
 *  
 * @param self A pointer to the list that will be tested.
 *
 * @param callback A pointer to a function with an interface `int (struct SinglyLinkedListNode *)` that will be called on every node to test if it passes the condition.
 *
 * @return `1` if some nodes pass the test, `0` otherwise.
 *
 * ## Example Usage
 *
 * ```c
 * struct SinglyLinkedList *my_list = SinglyLinkedList_create();
 * // Add elements to my_list...
 *
 * // Define a callback function to test if some nodes meet a condition (example: check if any value is negative).
 * int is_negative(struct SinglyLinkedListNode *current_node) {
 *     int *value = (int *)current_node->data;
 *     if (value != NULL) {
 *         return (*value < 0);
 *     }
 *     return 0; 
 * }
 *
 * int result = SinglyLinkedList_some(my_list, &is_negative);
 * // If result is 1, it means some nodes have negative values in the list.
 * ```
 */

 int SinglyLinkedList_some(struct SinglyLinkedList *self, int (*callback)(struct SinglyLinkedListNode *)) {
	for (struct SinglyLinkedListNode *current_node = self->head; current_node != NULL;
			current_node = current_node->next) {
		if (callback(current_node)) {
			return 1;
		}
	}
	return 0;
}

/**
 * @ingroup SinglyLinkedList
 * @brief Instance method that tests whether every node of the list passes the test provided by the given function.
 *
 * `int SinglyLinkedList_every(struct SinglyLinkedList *self, int (*callback)(struct SinglyLinkedListNode *))` tests whether every node in the given list passes the test provided by the callback function. It iterates through the list, invoking the callback function on each node. If the callback function returns a non-zero value for every node, this function returns `1`, indicating that every node passes the test. Otherwise, it returns `0`, indicating that at least one node does not pass the test.
 *
 * @param self A pointer to the list that will be tested.
 *
 * @param callback A pointer to a function with an interface `int (struct SinglyLinkedListNode *)` that will be called on every node to test if it passes the condition.
 *
 * @return `1` if every node passes the test, `0` otherwise.
 *
 * ## Example Usage
 *
 * ```c
 * struct SinglyLinkedList *my_list = SinglyLinkedList_create();
 * // Add elements to my_list...
 *
 * // Define a callback function to test if every node meets a condition (example: check if all values are positive).
 * int is_positive(struct SinglyLinkedListNode *current_node) {
 *     int *value = (int *)current_node->data;
 *     if (value != NULL) {
 *         return (*value >= 0);
 *     }
 *     return 0; 
 * }
 *
 * int result = SinglyLinkedList_every(my_list, &is_positive);
 * // If result is 1, it means SinglyLinkedList_every node has a non-negative value in the list.
 * ```
 */

 int SinglyLinkedList_every(struct SinglyLinkedList *self, int (*callback)(struct SinglyLinkedListNode *)) {
	for (struct SinglyLinkedListNode *current_node = self->head; current_node != NULL;
			current_node = current_node->next) {
		if (!callback(current_node)) {
			return 0;
		}
	}
	return 1;
}

