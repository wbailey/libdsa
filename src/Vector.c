#include <stdlib.h>

#include "./Vector.h"

#define VECTOR_MIN_LENGTH 10

static int _Vector_resize_larger(struct Vector *self);
static int _Vector_resize_smaller(struct Vector *self);
static int _Vector_check_index_in_boundaries(struct Vector * self, int index);

/**
  * @ingroup Vector
  * @brief Tests if a Vector needs to be resized larger, and resizes it if necessary
  *
  * @param self The Vector to resize
  *
  * @return 1 if the Vector was either judged to not need resizing or was resized successfully, 0 otherwise indicates a memory allocation failure 
  *
  */

static int _Vector_resize_larger(struct Vector *self) {
	if (self->length == self->_max_allocated) {
		size_t new_max_allocated = self->_max_allocated * 2;
		 void **new_data = realloc(self->_data, new_max_allocated * sizeof(void *));
		 if (!new_data) {
		 	return 0;
		 }
		self->_max_allocated = new_max_allocated;
		self->_data = new_data;
	}
	return 1;
}

/**
  * @ingroup Vector
  * @brief Tests if a Vector needs to be resized smaller, and resizes it if necessary
  *
  * @param self The Vector to resize
  *
  * @return 1 if the Vector was either judged to not need resizing or was resized successfully, 0 otherwise indicates a memory allocation failure 
  *
  */

static int _Vector_resize_smaller(struct Vector *self) {	
	if (self->length < (self->_max_allocated / 2)) {
		size_t new_max_allocated = self->_max_allocated / 2;
		 void **new_data = realloc(self->_data, new_max_allocated * sizeof(void *));
		 if (!new_data) {
		 	return 0;
		 }
		self->_max_allocated = new_max_allocated;
		self->_data = new_data;
	}
	return 1;
}

/**
  * @ingroup Vector
  * @brief Tests if a given index is within the boundaries of a Vector
  *
  * @param self The Vector to check
  * @param index The index to check
  *
  * @return 1 if the index is within the boundaries of the Vector, 0 otherwise
  *
  */

static int _Vector_check_index_in_boundaries(struct Vector * self, int index) {
	if (index < self->length && index >= 0) {
		return 1;
	}
	return 0;
}
/**
  * @ingroup Vector
  * @brief Creates a new Vector
  * 
  * @return A new Vector
  *
  * ## Example Usage
  * 
  * ```c
  * // Create a new Vector
  * struct Vector *my_vector = Vector_create();
  * my_vector->length; // 0
  * my_vector->_data; // NULL
  * ```
  */

struct Vector *Vector_create() {
	struct Vector *self = malloc(sizeof(struct Vector));
	if (!self) {
		return NULL;
	}
	self->_data = malloc(sizeof(void *) * VECTOR_MIN_LENGTH);
	if (!self->_data) {
		free(self);
		return NULL;
	}
	self->length = 0;
	self->_max_allocated = VECTOR_MIN_LENGTH;
	return self;
}

/**
  * @ingroup Vector
  * @brief Destroys a Vector
  *
  * @param self The Vector to destroy
  *
  * ## Example Usage
  *
  * ```c
  * // Create a new Vector
  * struct Vector *my_vector = Vector_create();
  * // Destroy the Vector
  * Vector_destroy(my_vector);
  * ```
  */

void Vector_destroy(struct Vector *self) {
	if (self) {
		free(self->_data);
		free(self);
	}
}

/**
  * @ingroup Vector
  * @brief Checks if a Vector is empty
  *
  * @param self The Vector to check
  *
  * @return 1 if the Vector is empty, 0 otherwise
  *
  * ## Example Usage
  *
  * ```c
  * // Create a new Vector
  * struct Vector *my_vector = Vector_create();
  * // Check if the Vector is empty
  * Vector_is_empty(my_vector); // 1
  * // Append data to the Vector
  * Vector_append(my_vector, "Hello");
  * // Check if the Vector is empty
  * Vector_is_empty(my_vector); // 0
  * ```
  */

int Vector_is_empty(struct Vector *self) {
	return self->length == 0;
}

/**
  * @ingroup Vector
  * @brief Appends data to the end of a Vector
  *
  * @param self The Vector to append to
  * @param data The data to append
  *
  * ## Example Usage
  *
  * ```c
  * // Create a new Vector
  * struct Vector *my_vector = Vector_create();
  * // Append data to the Vector
  * Vector_append(my_vector, "Hello");
  * my_vector->length; // 1
  * Vector_get(my_vector, 0); // "Hello"
  * ```
  */

int Vector_append(struct Vector *self, void *data) {
	if (!_Vector_resize_larger(self)) {
		return 0;
	}
	self->_data[self->length++] = data;
	return 1;
}

/**
  * @ingroup Vector
  * @brief Prepends data to the start of a Vector
  *
  * @param self The Vector to prepend to
  * @param data The data to prepend
  *
  * ## Example Usage
  *
  * ```c
  * // Create a new Vector
  * struct Vector *my_vector = Vector_create();
  * // Prepend data to the Vector
  * Vector_prepend(my_vector, "Hello");
  * my_vector->length; // 1
  * Vector_get(my_vector, 0); // "Hello"
  * ```
  */

int Vector_prepend(struct Vector *self, void *data) {
	if (!_Vector_resize_larger(self)) {
		return 0;
	}
	void *prev = self->_data[0];
	for (int i = 1; i < self->length + 1; i++) {
		void *swap = self->_data[i];
		self->_data[i] = prev;
		prev = swap;
	}
	self->_data[0] = data;
	self->length++;
	return 1;
}

/**
  * @ingroup Vector
  * @brief Pops the data at the end of a Vector
  *
  * @param self The Vector to pop from
  *
  * @return The data at the end of the Vector, or NULL if the Vector is empty
  *
  * ## Example Usage
  *
  * ```c
  * // Create a new Vector
  * struct Vector *my_vector = Vector_create();
  * // Append data to the Vector
  * Vector_append(my_vector, "Hello");
  * // Pop the data at the end of the Vector
  * int *(int *) tail = Vector_pop_tail(my_vector); // "Hello"
  * free(tail); // caller must free the memory
  * // Pop the data at the end of the Vector
  * Vector_pop_tail(my_vector); // NULL
  * ```
  */

void *Vector_pop_tail(struct Vector *self){
	if (Vector_is_empty(self)) {
		return NULL;
	}
	void *temp = malloc(sizeof(void *));
	temp = self->_data[self->length - 1];
	self->_data[self->length - 1] = NULL;
	self->length--;
	if (!_Vector_resize_smaller(self)) {
		return 0;
	}
	return temp;
}

/**
  * @ingroup Vector
  * @brief Pops the data at the start of a Vector
  *
  * @param self The Vector to pop from
  *
  * @return The data at the start of the Vector, or NULL if the Vector is empty
  *
  * ## Example Usage
  *
  * ```c
  * // Create a new Vector
  * struct Vector *my_vector = Vector_create();
  * // Append data to the Vector
  * Vector_append(my_vector, 100);
  * // Pop the data at the start of the Vector
  * int *(int *) head = Vector_pop_head(my_vector); // 100
  * free(head); // caller must free the memory
  * // Pop the data at the start of the Vector
  * Vector_pop_head(my_vector); // NULL
  * ```
  */

void *Vector_pop_head(struct Vector *self) {
	if (Vector_is_empty(self)) {
		return NULL;
	}
	void *temp = malloc(sizeof(void *));
	temp = self->_data[0];
	for (int i = 1; i < self->length; i++) {
		self->_data[i - 1] = self->_data[i];
	}
	self->_data[self->length - 1] = NULL;
	self->length--;
	if(!_Vector_resize_smaller(self)) {
		return 0;
	}
	return temp;	
}

/**
  * @ingroup Vector
  * @brief Clears a Vector
  *
  * @param self The Vector to clear
  *
  * ## Example Usage
  *
  * ```c
  * // Create a new Vector
  * struct Vector *my_vector = Vector_create(); 
  * // Append data to the Vector
  * Vector_append(my_vector, 10);
  * // Clear the Vector
  * Vector_clear(my_vector);
  * my_vector->length; // 0
  * my_vector->_data; // NULL
  * ```
  */

int Vector_clear(struct Vector *self){
	void **new_data = realloc(self->_data, VECTOR_MIN_LENGTH * sizeof(void *));
	if (!new_data) { 
		return 0;
	}
	self->length = 0;
	self->_max_allocated = VECTOR_MIN_LENGTH;
	self->_data = new_data;
	return 1;

}

/** 
  * @ingroup Vector
  * @brief Gets the data at a given index.
  *
  * @param self The Vector to get the data from
  * @param index The index to get the data from
  *
  * @return The data at the given index, or NULL if the index is out of bounds
  *
  * ## Example Usage
  *
  * ```c
  * // Create a new Vector
  * struct Vector *my_vector = Vector_create();
  * // Append data to the Vector
  * Vector_append(my_vector, "Hello");
  * // Get the data at index 0
  * Vector_get(my_vector, 0); // "Hello"
  * // Get the data at index 1
  * Vector_get(my_vector, 1); // NULL
  * ```
  */

void *Vector_get(struct Vector *self, int index){
	if (!_Vector_check_index_in_boundaries(self, index)) {
		return NULL;
	}
	return self->_data[index];
}

/**
  * @ingroup Vector
  * @brief Sets the data at a given index.
  *
  * @param self The Vector to set the data in
  * @param index The index to set the data at
  * @param data The data to set
  *
  * @return 1 if the data was set, 0 if the index is out of bounds
  *
  * ## Example Usage
  *
  * ```c
  * // Create a new Vector
  * struct Vector *my_vector = Vector_create();
  * // Append data to the Vector
  * Vector_append(my_vector, 100);
  * // Set the data at index 0
  * Vector_set(my_vector, 0, 200);
  * Vector_get(my_vector, 0); // 200
  * // Set the data at index 1
  * if (!Vector_set(my_vector, 40, 300)) {
  *     printf("Index out of bounds\n");
  * }
  * ```
  */

int Vector_set(struct Vector *self, int index, void *data) {
	if (!_Vector_check_index_in_boundaries(self, index)) {
		return 0;
	}
	self->_data[index] = data;
	return 1;
}

/**   
  * @ingroup Vector 
  * @brief Inserts data at a given index.
  *
  * @param self The Vector to insert the data into
  * @param index The index to insert the data at
  * @param new_data The data to insert
  * 
  * @return 1 if the data was inserted, 0 if the index is out of bounds or memory allocation has failed.
  *
  * ## Example Usage
  *
  * ```c
  * // Create a new Vector
  * struct Vector *my_vector = Vector_create();
  * // Append data to the Vector
  * Vector_append(my_vector, 100);
  * // Insert data at index 0
  * Vector_insert(my_vector, 0, 200);
  * Vector_get(my_vector, 0); // 200
  * Vector_get(my_vector, 1); // 100
  * // Insert data out of bounds
  * if (!Vector_insert(my_vector, 40, 300)) {
  *     printf("Index out of bounds\n");
  * }
  * ```
  */

int Vector_insert(struct Vector *self, int index, void *new_data) {
	if (!_Vector_check_index_in_boundaries(self, index)) {
			return 0;
		}
	if (!_Vector_resize_larger(self)) {
		return 0;
	}
	for (int i = self->length - 1; i >= index; i--) {
		self->_data[i + 1] = self->_data[i];
	}
	self->_data[index] = new_data;
	self->length++;
	return 1;
}

/**
  * @ingroup Vector
  * @brief Gets the index of a given data
  *
  * @param self The Vector to search
  * @param compare The comparison function to use
  *
  * @return The index of the data, or -1 if the data is not found	
  *
  * ```c
  * // define a comparison function
  * int compare(void *a) {
  *     return *(int *)a == 5;
  * }
  * // Create a new Vector
  * struct Vector *my_vector = Vector_create();
  * // Append some data to the vector
  * for (int i = 0; i < 10; i++) {
  *    int *data = malloc(sizeof(int));
  *    *data = i;
  *    Vector_append(my_vector, data);
  * }
  * // Get the index of the data
  * Vector_index_of(my_vector, compare); // 4
  * ```
  */

int Vector_index_of(struct Vector *self, int (compare)(void *cureent_data)) {
	for (int i = 0; i < self->length; i++) {
		if (compare(self->_data[i])) {
			return i;
		}
	}
	return -1;
}

/**
  *
  */

void *Vector_remove(struct Vector *self, int index){
	if (!_Vector_check_index_in_boundaries(self, index)) {
		return NULL;
	}
	void *data = malloc(sizeof(void *));
	data = self->_data[index];
	for (int i = index + 1; i < self->length; i++) {
		self->_data[i - 1] = self->_data[i];
	}
	self->length--;
	if (!_Vector_resize_smaller(self)) {
		return NULL;
	}
	return data;
}

/**
  * @ingroup Vector
  * @brief Removes a range of data from a Vector
  *
  * @param self The Vector to remove data from
  * @param start The start index of the range to remove
  * @param end The end index of the range to remove
  *
  * @return The removed data, or NULL if the range is out of bounds
  *
  * ## Example Usage
  *
  * ```c
  * // Create a new Vector
  * struct Vector *my_vector = Vector_create();
  * // Append data to the Vector
  * for (int i = 0; i < 10; i++) {
  *     int *data = malloc(sizeof(int));
  *     *data = i;
  *     Vector_append(my_vector, data);
  * }
  * // Remove a range of data
  * Vector_remove_range(my_vector, 2, 5); // [2, 3, 4, 5]
  * ```
  */

struct Vector *Vector_remove_range(struct Vector *self, int start, int end) {
	
}

/**
  *
  */

void *Vector_reverse(struct Vector *self){}

/**
  *
  */

void *Vector_sort(struct Vector *self, int (*compare)(void *, void *)){}

/**
  *
  */

struct Vector *Vector_copy(struct Vector *self){}

/**
  *
  */

struct Vector *Vector_concat(struct Vector *self, struct Vector *other){}

/**
  *
  */

void Vector_for_each(struct Vector *self, void (*callback)(void *)){}

/**
  *
  */

struct Vector *Vector_map(struct Vector *self, void *(*callback)(void *)){}

/**
  *
  */

struct Vector *Vector_filter(struct Vector *self, int (*callback)(void *)){}

/**
  *
  */

void *Vector_reduce(struct Vector *self, void *(*callback)(void *, void *), void* initial_value){}

/**
  *
  */

int Vector_some(struct Vector *self, int (*callback)(void *)){}

/**
  *
  */

int Vector_every(struct Vector *self, int (*callback)(void *)){}

