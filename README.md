# LibDSA

A collection of datastructures and algorithms written in c.

The documentation for the library can be viewed [here](https://wbailey.gitlab.io/libdsa/).


## Usage

`TODO`: add install to path instructions.

* clone this repo and cd into it.
* `touch main.c` and add the following code:
```c
#include "./src/SinglyLinkedList.h"
#include <stdio.h>

void print_char_node(struct SinglyLinkedListNode *current_node);

int main (int argc, char *argv[]) {
	char str[] = "Hello, libDSA";
	struct SinglyLinkedList *test_list = SinglyLinkedList_create();

	for (int i = 0; str[i] != '\0'; i++) {
		SinglyLinkedList_append(test_list, (void*) &str[i]);
	}

	SinglyLinkedList_for_each(test_list, &print_char_node);

	SinglyLinkedList_destroy(test_list);

	return 0;
}

void print_char_node(struct SinglyLinkedListNode *current_node) {
	printf("%c", *(char*) current_node->data);
	if (current_node->next == NULL) {
		printf("\n");
	}
}
```
* build and run with `$ gcc main.c -L $(pwd)/lib -I $(pwd)/lib  -l DSA && ./a.out`
* output should be "Hello, libDSA".

## Building

### Dependencies

The following tools are needed to build:

- [gnu make](https://www.gnu.org/software/make/) - the build system.
- [criterion](https://github.com/Snaipe/Criterion) - Unit testing framework.
- [Doxygen](https://www.doxygen.nl/) - Builds documentation.
- [graphviz](https://graphviz.org/) - Dependency of Doxygen.

The Doxygen theme is [Doxygen Awesome](https://jothepro.github.io/doxygen-awesome-css/index.html)

### Build Commands

* `$ make` - builds a with debug flags and runs all tests
* `$ make relaese` - builds without debug flags, runs all tests
* `$ make test` - runs tests only, rebuilds as needed
* `$ make test-clean` - removes test binaries
* `$ make docs` - builds docs from doxygen comments
* `$ make docs-clean` - removes docs
* `$ make clean` - removes build objects and binaries
* `$ make submit` - (WIP) zips binary

### Testing

* `./tests/bin/<test_name>` - run a test individually
* `./tests/bin/<test_name> --verbose` - run a test with verbose output
* `./tests/bin/<test_name> -h` - Criterion help

## Todos

The Current source todo list can be found [here](https://wbailey.gitlab.io/libdsa/todo.html).

List of Datastructures to implement:
- doubly linked list 
- circularly linked list 
- stack
- queue
- double ended queue
- hash table
- binary search tree
- heap
- graph
- disjoint set
- segment tree
- matrix
- trie
- B-Tree and B+ Tree
- Red-Black Tree
- AVL Tree
- Bidirectional Map
- set
- map
- weakmap
- priority queue
- Circular Buffer


