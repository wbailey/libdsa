var group__SinglyLinkedList =
[
    [ "SinglyLinkedList", "structSinglyLinkedList.html", null ],
    [ "SinglyLinkedListNode", "structSinglyLinkedListNode.html", null ],
    [ "CREATE_SINGLY_LINKED_LIST_COPY_FUCNTION", "group__SinglyLinkedList.html#ga55571d8b66927f019ef10a4c7892dfe4", null ],
    [ "CREATE_SINGLY_LINKED_LIST_PRETTY_PRINT_FUNCTION", "group__SinglyLinkedList.html#gaaf3cacb51c5fd9b5acb1978a6c201898", null ],
    [ "SinglyLinkedList_append", "group__SinglyLinkedList.html#ga5fa3fcd815aedecee46d514aac31b0fd", null ],
    [ "SinglyLinkedList_clear", "group__SinglyLinkedList.html#ga2d80b0421d46ca6e862ec9aacb9fa70a", null ],
    [ "SinglyLinkedList_copy", "group__SinglyLinkedList.html#ga407dc7241bef37b0111b7345680bb3b2", null ],
    [ "SinglyLinkedList_create", "group__SinglyLinkedList.html#ga8259754ac486e26f8061e9c7fdd1f2e3", null ],
    [ "SinglyLinkedList_destroy", "group__SinglyLinkedList.html#gaf12231033f304171aac77ceb9d2b30b7", null ],
    [ "SinglyLinkedList_every", "group__SinglyLinkedList.html#ga19d7bcaffd7009e620ec1a8a3a7baeb8", null ],
    [ "SinglyLinkedList_filter", "group__SinglyLinkedList.html#gaff2f087a24a4de4c01115d957d1f17ec", null ],
    [ "SinglyLinkedList_for_each", "group__SinglyLinkedList.html#ga0072dbc17eb51db36579d3fd3b42f70b", null ],
    [ "SinglyLinkedList_is_empty", "group__SinglyLinkedList.html#gaff74d9bf29231a08fa7eba675bc7dfe4", null ],
    [ "SinglyLinkedList_map", "group__SinglyLinkedList.html#ga2e25f0e66bdfcb58b7266a224186f17c", null ],
    [ "SinglyLinkedList_pop_head", "group__SinglyLinkedList.html#ga32e4addeb4255b3742db82cf00f67119", null ],
    [ "SinglyLinkedList_pop_tail", "group__SinglyLinkedList.html#ga563ee0839403cb160b98fcf88efcc506", null ],
    [ "SinglyLinkedList_prepend", "group__SinglyLinkedList.html#gabe2c787c6251bd2c6c1cbc78ff371b90", null ],
    [ "SinglyLinkedList_some", "group__SinglyLinkedList.html#ga19473b4270502d0c0a7e79252b8af1ba", null ]
];