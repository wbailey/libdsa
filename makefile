CC = gcc
CFLAGS = -g -Wall -Werror

# directories

SRC_DIR = src
OBJ_DIR = obj
BIN_DIR = bin
LIB_DIR = lib
TEST_DIR = tests

# output

# the name of the library
MAIN = libDSA
LIB = $(LIB_DIR)/$(MAIN).a
SUBMIT_NAME = $(MAIN).zip

SRCS = $(wildcard $(SRC_DIR)/*.c)
TESTS = $(wildcard $(TEST_DIR)/*.c)
OBJS = $(patsubst $(SRC_DIR)/%.c, $(OBJ_DIR)/%.o, $(SRCS))
TEST_BINS = $(patsubst $(TEST_DIR)/%.c, $(TEST_DIR)/$(BIN_DIR)/%, $(TESTS))

all: $(LIB) test 

release: CFLAGS = -Wall -O3 -DNDEBUG # O3 all optimizations
release: clean
release: $(LIB) test 

$(LIB): $(LIB_DIR) $(OBJ_DIR) $(OBJS)
	$(RM) $(LIB)
	ar -cvrs $(LIB) $(OBJS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(SRC_DIR)/%.h
	$(CC) $(CFLGAGS) -c $< -o $@

$(OBJ)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(TEST_DIR)/$(BIN_DIR)/%: $(TEST_DIR)/%.c
	$(CC) $(CFLAGS) $< $(OBJS) -o $@ -lcriterion

$(TEST_DIR)/$(BIN_DIR):
	mkdir -p $@

$(OBJ_DIR):
	mkdir -p $@

$(LIB_DIR):
	mkdir -p $@

# ./tests/bin/funcs_test --verbose -h
.PHONY: test
test: $(LIB) $(TEST_DIR)/$(BIN_DIR) $(TEST_BINS)
	for test in $(TEST_BINS) ; do ./$$test; done

.PHONY: test-clean
test-clean:
	$(RM) -r $(TEST_DIR)/$(BIN_DIR)

.PHONY: docs-clean
docs-clean:
	$(RM) -r public

.PHONY: docs
docs: docs-clean
	doxygen Doxyfile
	mv html public

clean: test-clean
	$(RM) -r $(OBJ_DIR) $(LIB_DIR)

submit:
	$(RM) $(SUBMIT_NAME)
	zip $(SUBMIT_NAME) $(LIB) 
